from app import app
from flask import render_template
from forms import LoginForm

@app.route('/')
@app.route('/index')
def index():
     return render_template('index.html', title='Home', user='Aloo Kisaan')

@app.route('/query')
def register():
     form = LoginForm()
     return render_template('register.html', title='Query', form=form)

@app.route('/farmer')
def farmer():
     return render_template('farmer.html', title='Farmer Form')